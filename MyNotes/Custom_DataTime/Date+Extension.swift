

import Foundation

extension Date {
    func format() -> String {
        let formatter = DateFormatter()
        if Calendar.current.isDateInToday(self) {
            formatter.dateFormat = "dd-MM-yyyy"
            
        } else {
            formatter.dateFormat = "h:mm a"
        }
        return formatter.string(from: self)
    }
}

